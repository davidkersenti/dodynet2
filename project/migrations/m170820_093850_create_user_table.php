<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_table`.
 */
class m170820_093850_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
       public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'password' => $this->string(),
            'auth_Key' => $this->string(),
            'phoneNumber' => $this->integer(),
            'address' => $this->string(),
        ]);
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_table');
    }
}
